#!/bin/bash
#=
PROJECT_DIR=$(dirname $(dirname $(realpath "${BASH_SOURCE[0]}")))
if [ -z "$JULIA" ]; then JULIA=julia; fi
exec $JULIA --project="$PROJECT_DIR" "${BASH_SOURCE[0]}" "$@"
=#

using LazyArtifacts

projectdir = dirname(Base.active_project())

function pretrained_models(name)
    artifact = @artifact_str(name)
    src = @artifact_str("$name/pretrained_models")
    dst = mkpath(joinpath(projectdir, "pretrained_models"))
    for filename in readdir(src; join=false)
        mv(joinpath(src, filename), joinpath(dst, filename))
    end
    rm(artifact; recursive=true, force=true)
end

function models(name)
    src = artifact = @artifact_str(name)
    dst = mkpath(joinpath(projectdir, "models"))
    for filename in readdir(src; join=false)
        mv(joinpath(src, filename), joinpath(dst, filename))
    end
    rm(artifact; recursive=true, force=true)
    @assert isdir(joinpath(dst, name))
end

function main(args=ARGS)
    if isempty(args)
        print("missing pretrained model name")
        exit()
    elseif length(args) == 1 && args[1] == "default"
        args = ["20230524-6behaviors-25", "20230524-hunch-25", "20230524-roll-25"]
        cd(mkpath(joinpath(projectdir, "pretrained_models"))) do
            symlink("20230524-6behaviors-25", "default"; dir_target=true)
        end
    end
    for arg in args
        pretrained_models(arg)
    end
    for arg in ("20230311", "20230311-0")
        models(arg)
    end
end

main()
