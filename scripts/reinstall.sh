#!/bin/bash

# this simple series of commands forces Poetry to update its local dependencies;
# useful with a local TaggingBackends for example (pyproject.toml must be updated first).

rm -rf $(poetry env info -p)
poetry env use python3.8
rm -f poetry.lock
poetry install -v

if [ "$(git branch --show-current)" = "design" ]; then
# register the venv with Jupyter
envname=$(basename $(poetry env info -p))
poetry run python -m ipykernel install --user --name $envname
sed -i "s|],|],\n \"env\": {\n  \"JULIA_PROJECT\": \"$(realpath ../TaggingBackends)\"\n },|" ~/.local/share/jupyter/kernels/${envname,,}/kernel.json
else
echo 'Note: only the "design" branch exports its environment as a kernel for Jupyter'
fi

# if pyproject.toml was not updated in the first place:
#poetry add ../TaggingBackends -v
