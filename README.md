# MaggotUBA backend adapter

Wrapper project to allow the Nyx tagger UI to call [`MaggotUBA`](https://gitlab.pasteur.fr/nyx/maggotuba-core).

This project heavily depends on the [`TaggingBackends`](https://gitlab.pasteur.fr/nyx/TaggingBackends) package that drives the development of automatic tagging backends.

## Principle

MaggotUBA is an autoencoder trained on randomly sampled 20-time-step time segments drawn from the t5 and t15 databases (from t15 only for current default), with a computational budget of 1,000 training epochs (10,000 for current default).
In its original "unsupervised" or self-supervised form, it reconstructs series of spines from a compressed latent representation.

For the automatic tagging, the encoder is combined with a classifier.
On the same dataset, the combined encoder-classifier are (re-)trained to predict discrete behaviors.

## Taggers

### 6-behavior classification task

*6-behavior* refers to *run*, *cast*, *back*, *hunch*, *roll* and *stop_large*.

#### `20220418`

As a first prototype, the [`20220418`](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/tree/20220418) trained model is based on a simple random forest classifier, and only the classifier was trained; the encoder was not retrained.
See module [`maggotuba.models.randomforest`](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/blob/20220418/src/maggotuba/models/randomforest.py).

It was trained on the entire t5+t15 database. No interpolation was performed and the prototype does not properly handle data with different frame rates.

#### `20221005` and `20221005-1`

A second tagger called [`20221005`](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/tree/20221005) involves a classifier with dense layers, and the encoder was fine-tuned while training the combined encoder+classifier.
See modules [`maggotuba.models.trainers`](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/blob/20221005/src/maggotuba/models/trainers.py) and [`maggotuba.models.modules`](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/blob/20221005/src/maggotuba/models/modules.py).

This second tagger was dimensioned following a [parametric exploration for the 6-behavior classification task](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/blob/design/notebooks/parametric_exploration_6-behavior_classification.ipynb): 2-second time segments, 100-dimension latent space, 3 dense layers.

It was trained on a subset of 5000 files from the t5 and t15 databases. Spines were/are linearly interpolated at 10 Hz in each time segment individually.

The `20221005-1` tagger is identical. It was first thought to implement a post-prediction correction step referred to as *ABA -> AAA*, but actually did not.

#### `20221228`

This tagger combines 25 taggers based on a 1.5-s time window and 25 taggers based on a 3-s time window, and uses a voting approach (or bagging).
All simple taggers featured 25 latent dimensions and 1 dense layer (as classifier) only, following another [parametric exploration for the 6-behavior classification task](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/blob/design/notebooks/parametric_exploration_6-behavior_classification_2.ipynb).

This complex tagger was not distributed in any `latest` Docker image. See experimental image [0.7.2-20221228](https://hub.docker.com/layers/flaur/larvatagger/0.7.2-20221228/images/sha256-bac83d7ec499da9e868af893b58f8dd7b75317e0b16d99b1dcc1d456aac3c8a0?context=explore).

It runs 50 times slower and does not solve any of the issues of `20221005`.

### 7-behavior classification task

*7-behavior* refers to *run_large*, *cast_large*, *back_large*, *hunch_large*, *roll_large*, *stop_large* and *small_motion*.

#### `20230111`

As a stronger default tagger, the `small_motion` was reintroduced to lower the detection rate of hunches and rolls.

The `20230111` tagger uses a 2-s time window, features 25 latent dimensions and a single dense layer as classifier.
It applies a post-prediction rule referred to as *ABC -> AAC* that consists in correcting all single-step actions with the previous action.

#### `20230129`

Previous tagger `20230111` revealed a [temporal leakage issue](https://gitlab.pasteur.fr/nyx/larvatagger.jl/-/issues/88) that might have affected all previous taggers.

A similar tagger called `20230129` has been proposed to moderate this issue.
This tagger shares the same characteristics as `20230111` and differs in three important aspects:

* the number of training epochs was brought from 1,000 to 10,000 to let the original features be largely forgotten,
* the training stage involved more data: 1,200,235 time segments were used instead of 100,000; these data were unbalanced and training was performed with class weighting as per the newly introduced balancing strategy `auto` (see https://gitlab.pasteur.fr/nyx/larvatagger.jl/-/issues/92),
* pretraining and training data were drawn from t15 only (as opposed to previous taggers that were pretrained and trained with data from t15 and t5).

Note the last difference was not meant to improve performance. The `20230129` was trained this way to study its performance on t5, and was kept as is after it showed better properties (less temporal leakage, fewer hunches and rolls except on stimulus onset) on t5 data.

### 12-behavior classification task

*12-behavior* refers to *run_strong*, *cast_strong*, *back_strong*, *hunch_strong*, *roll_strong*, *stop_strong*, *run_weak*, *cast_weak*, *back_weak*, *hunch_weak*, *roll_weak* and *stop_weak*.

#### `20230311-0` and `20230311`

To achieve higher similarity in tag predictions with JBM's tagger output, the `20230129` pretrained encoder was used to train the `20230311-0` tagger on the 12-behavior classification. The `20230311` tagger is essentially the same tagger, with the predicted tags remapped onto the 7-behavior classification.

## Usage

For installation, see [TaggingBackends' README](https://gitlab.pasteur.fr/nyx/TaggingBackends/-/tree/dev#recommended-installation). However, note `MaggotUBA-adapter` is not supported by macOS or Windows because of its dependency on PyTorch 1.

A MaggotUBA-based tagger is typically called using the `poetry run tagging-backend` command from the root directory of the backend's project.

All the [command arguments supported by `TaggingBackends`](https://gitlab.pasteur.fr/nyx/TaggingBackends/-/blob/dev/src/taggingbackends/main.py) are also supported by `MaggotUBA-adapter`.

### Automatic tagging

For example, the `20230311` tagger can be called to generate labels with:

```
poetry run tagging-backend predict --model-instance 20230311
```

Note: since `TaggingBackends==0.10`, the `--skip-make-dataset` argument is default behavior. Pass `--make-dataset` instead to enforce the former default.

For the above command to work, the tracking data file must be placed (*e.g.* copied) in the `data/raw/20230311` directory, to be first created or cleared.

The resulting label file can be found as *data/processed/20230311/predicted.label*.
Like all *.label* files, this file should be stored as a sibling of the corresponding track data file (in the same directory).

Similarly, with an arbitrary tagger named, say *mytagger*, in the above explanation all occurrences of `20230311` or *20230311* must be replaced by the tagger's name.
For example, the input data file would go into *data/raw/mytagger*.

#### On HPC clusters

Simultaneously tagging multiple tracking data files result in file conflicts because the same data directories are used internally.
Use the *larvatagger.jl* script of the [LarvaTagger.jl](https://gitlab.pasteur.fr/nyx/larvatagger.jl) project instead, with argument `--data-isolation`.

### Retraining a tagger

#### The `train` command

A new model instance can be trained on a data repository with:

```
poetry run tagging-backend train --model-instance <tagger-name>
```

Similarly to the *predict* command, for this one to work, the data repository must be made available in the *data/raw/\<tagger-name\>* directory (again, to be created or cleared).

The above command will first load a pretrained model (`pretrained_models/default` in `MaggotUBA-adapter`) to determine additional parameters, such as whether to interpolate the spines or not and at which frequency, or the window length.

Beware that the default pretrained model may depend on the branch you are on.

The default pretrained model in the *20221005* branch involves linearly interpolating the spines at 10 Hz, and relies on a 20-time-step window (2 seconds). The dimensionality of the latent space is 100.

The default pretrained models in the *20230111*, *20230129*, *20230311*, *main* and *dev* branches similarly interpolate spines at 10 Hz and rely on a 20-time-step window (2 seconds), but feature 25 latent dimensions only.

Alternative pretrained models can be specified using the `--pretrained-model-instance` option.

The data files are discovered in the repository (more exactly in *data/raw/\<tagger-name\>*) and behavior tags are counted.
A subset of tags can be specified using the `--labels` option followed by a list of comma-separated tags.

A two-level balancing rule is followed to randomly select time segments and thus form a training dataset in the shape of a *larva_dataset* hdf5 file.
See also the [`make_dataset.py`](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/blob/dev/src/maggotuba/data/make_dataset.py) script.

Training operates in two steps, first pretraining the dense-layer classifier, second simultaneously fine-tuning the encoder and classifier.
See also the [`train_model.py`](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/blob/dev/src/maggotuba/models/train_model.py) script.

This generates a new sub-directory in the `models` directory of the `MaggotUBA-adapter` project, which makes the trained model discoverable for automatic tagging (*predict* command).

#### The `finetune` command

Alternatively, in cases with few but similar data, an already trained tagger can be further trained, instead of having the encoder part pretrained only.

This can be done with:
```
poetry run tagging-backend finetune --model-instance <new-tagger-name> --original-model-instance <reused-tagger-name>
```

See also the [`finetune_model.py`](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter/-/blob/dev/src/maggotuba/models/finetune_model.py) script.

Beware that the behavior labels must be compatible with the model instance. In particular the [label mapping feature](https://gitlab.pasteur.fr/nyx/larvatagger.jl/-/issues/103) can interfere, because the predicted labels differ from the actual labels the tagger actually generates in the first place, before the mapping applies.
If files with such mapped labels are used for fine-tuning, the `finetune` step may fail to sample a training dataset from these data.
Indeed, `finetune` loads the pre-mapping label definition from the model files and samples instances of these labels in the data.

This is relevant in cases where the `20230311` tagger is used to generate a first round of predictions that are manually corrected with the purpose of retraining (fine-tuning) a new tagger based on the corrected labelled data. The labels will not be compatible with the underlying classifier. Only `train` will apply.

The `202230311-0` tagger may be considered instead, if `finetune` may be used with the above-mentioned purpose.

### Embeddings

A trained encoder can be used to generate embeddings, or intermediate vectorized representations of the input track segments. Embeddings are useful for example to compare between different experimental conditions in an annotation-agnostic fashion.

Although in principle both pre-trained and fine-tuned encoders can be used, a model available in the `models` directory is required:
```
poetry run tagging-backend embed --model-instance 20230311
```
To apply the encoder of a model in the `pretrained_models` directory, copy the corresponding directory in `models`.

Similarly to the other commands, input data files are expected in the data/raw directory corresponding to the designated model instance (in the above example, in data/raw/20230311).

The above command produces an `embeddings.h5` file in the data/processed/20230311 directory.

The `embeddings.h5` file is an HDF5 file containing several arrays that all feature as many elements or rows as
embedded/projected data points. This file is structured as follows:
```
├── run_id         <- 1D array, typically of strings; id of the tracking data file or assay or run.
├── track_id       <- 1D array of integers; id of the track or larva.
├── time           <- 1D array of floats; timestamp of the time step or time segment center.
└── embedding      <- 2D array of floats; coordinates in the latent space.
```
This format is not compatible with the `clustering.cache` file used by [MaggotUBA's ToMATo UI](https://github.com/DecBayComp/Detecting_subtle_behavioural_changes/blob/ee73f0dd294a991322a0eec8f6ce69488c7a1f9a/maggotuba/src/maggotuba/cli/cli_model_clustering.py#L129-L164).

Track ids are not unique across runs. Similarly, times do not share a common time origin across runs.

To visualize the embeddings, the `embedding` matrix can be loaded and transformed with methods like UMAP:
```
import h5py
import umap

with h5py.File('embeddings.h5', 'r') as f:
    embedding = f['embedding'][...]

embedding2d = umap.UMAP().fit_transform(embedding)
```
