#!/bin/bash

[ -f ./pyproject.toml ] || cd ..
[ -f ./pyproject.toml ] || exit "failed to locate MaggotUBA-adapter project"

# pick any data file;
# could fetch and untar data file https://gitlab.pasteur.fr/nyx/artefacts/-/raw/master/PlanarLarvae/trx.mat.tgz?inline=false instead, for example
datapath="t15/FCF_attP2_1500062@UAS_Chrimson_Venus_X_0070/r_LED100_30s2x15s30s#n#n#n@100/20140723_113810"
reldatapath="data/raw/subset4/$datapath"
if ! [ -d "$reldatapath" ]; then
mkdir -p "$reldatapath"
cp "../data/$datapath/trx.mat" "$reldatapath"
fi

modeldir=pretrained_models/untrained
if ! [ -d "$modeldir" ]; then
mkdir -p "$modeldir"
cat << "EOT" > "${modeldir}/autoencoder_config.json"
{
  "project_dir": "",
  "seed": 100,
  "exp_name": "",
  "data_dir": "",
  "raw_data_dir": "",
  "log_dir": "",
  "exp_folder": "",
  "config": "",
  "num_workers": 4,
  "n_features": 10,
  "len_traj": 20,
  "len_pred": 20,
  "dim_latent": 100,
  "activation": "relu",
  "enc_filters": [
    128,
    64,
    32,
    32,
    32,
    16
  ],
  "dec_filters": [
    128,
    64,
    32,
    32,
    32,
    16
  ],
  "enc_kernel": [
    [
      5,
      1
    ],
    [
      1,
      20
    ],
    [
      5,
      1
    ],
    [
      1,
      20
    ],
    [
      5,
      1
    ],
    [
      1,
      20
    ]
  ],
  "dec_kernel": [
    [
      1,
      20
    ],
    [
      5,
      1
    ],
    [
      1,
      20
    ],
    [
      5,
      1
    ],
    [
      1,
      20
    ],
    [
      5,
      1
    ]
  ],
  "bias": false,
  "enc_depth": 4,
  "dec_depth": 4,
  "init": "kaiming",
  "n_clusters": 2,
  "dim_reduc": "UMAP",
  "optim_iter": 1000,
  "pseudo_epoch": 100,
  "batch_size": 128,
  "lr": 0.005,
  "loss": "MSE",
  "cluster_penalty": null,
  "cluster_penalty_coef": 0.0,
  "length_penalty_coef": 0.0,
  "grad_clip": 100.0,
  "optimizer": "adam",
  "target": [
    "past",
    "present",
    "future"
  ],
  "spine_interpolation": "linear",
  "frame_interval": 0.1,
  "swap_head_tail": false,
  "load_state": false
}
EOT
cp pretrained_models/default/best_validated_encoder.pt "$modeldir"
fi

rm -rf models/subset4

JULIA_PROJECT=$(realpath ../TaggingBackends) poetry run tagging-backend train --model-instance subset4 --pretrained-model-instance untrained --labels run,cast,back,hunch
