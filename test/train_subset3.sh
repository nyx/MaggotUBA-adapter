#!/bin/bash

[ -f ./pyproject.toml ] || cd ..
[ -f ./pyproject.toml ] || exit "failed to locate MaggotUBA-adapter project"

backend=$(pwd)

mkdir -p data/raw/subset3
pushd data/raw/subset3

if ! [ -d t15 ]; then
	[ -d ~/Zeus/hecatonchire/screens ] || exit "cannot find screens/t15"
	rm -rf *
	ls ~/Zeus/hecatonchire/screens/t15/FCF_attP2_1500062@UAS_Chrimson_Venus_X_0070/r_LED100_30s2x15s30s\#n\#n\#n@100/2014*/trx.mat | cut -d/ -f7-10 | xargs -I % bash -c 'mkdir -p %; cp ~/Zeus/hecatonchire/screens/%/trx.mat %/'
	chmod a-x */*/*/*/trx.mat
fi

popd

poetry run tagging-backend train --model-instance subset3 --labels run,cast,back,roll,hunch,stop_large
