from taggingbackends.data.labels import Labels
from taggingbackends.features.skeleton import get_5point_spines
from maggotuba.models.trainers import MaggotTrainer, MultiscaleMaggotTrainer, MaggotBagging, new_generator
import numpy as np
import logging
import os.path
import re


tracking_data_file_extensions = ('.spine', '.outline', '.csv', '.mat', '.hdf5')


def predict_model(backend, **kwargs):
    """
    This function generates predicted labels for all the input data.

    It supports single *larva_dataset* hdf5 files, or (possibly multiple) track
    data files.

    Input files are expected in `data/interim` or `data/raw`.

    The predicted labels are saved in `data/processed`, in `predicted.label`
    files, following the same directory structure as in `data/interim` or
    `data/raw`.
    """
    if kwargs.pop('debug', False):
        logging.root.setLevel(logging.DEBUG)

    # we pick files in `data/interim` if any, otherwise in `data/raw`
    input_files = backend.list_interim_files(group_by_directories=True)
    if not input_files:
        input_files = backend.list_input_files(group_by_directories=True)
    assert 0 < len(input_files)

    # initialize output labels
    input_files_and_labels = backend.prepare_labels(input_files, single_input=True,
        allowed_file_extensions=tracking_data_file_extensions)
    assert 0 < len(input_files_and_labels)

    # load the model
    model_files = backend.list_model_files()
    config_files = [file for file in model_files
                    if file.name.endswith('config.json')]
    if len(config_files) == 0:
        raise RuntimeError(f"no config files found for tagger: {backend.model_instance}")
    single_encoder_classifier = len(config_files) == 2
    config_files = [file
                    for file in config_files
                    if file.name == 'clf_config.json']
    if len(config_files) == 0:
        raise RuntimeError(f"no classifier config files found; is {backend.model_instance} tagger trained?")
    elif len(config_files) == 1:
        config_file = config_files[0]
        if single_encoder_classifier:
            model = MaggotTrainer(config_file)
        else:
            model = MultiscaleMaggotTrainer(config_file)
    else:
        model = MaggotBagging(config_files)

    # call the `predict` logic on the input data files
    if len(input_files) == 1:
        input_files = next(iter(input_files.values()))
        if len(input_files) == 1:
            file = input_files[0]
            if file.name.startswith("larva_dataset_") and file.name.endswith(".hdf5"):
                return predict_larva_dataset(backend, model, file, **kwargs)
    predict_individual_data_files(backend, model, input_files_and_labels)


def predict_individual_data_files(backend, model, input_files_and_labels):
    from taggingbackends.data.trxmat import TrxMat
    from taggingbackends.data.chore import load_spine
    import taggingbackends.data.fimtrack as fimtrack

    for input_files, labels in input_files_and_labels.values():
        labels.load_model_config(model.clf_config)
        done = False
        for file in input_files:

            # load the input data (or features)
            if done:
                logging.info(f"ignoring file: {file.name}")
                continue
            elif file.name.endswith(".outline"):
                # skip to spine file
                logging.info(f"ignoring file: {file.name}")
                continue
            elif file.name.endswith(".spine"):
                spine = load_spine(file)
                run = spine["date_time"].iloc[0]
                larvae = spine["larva_id"].values
                t = spine["time"].values
                data = spine.iloc[:,3:].values
            elif file.name.endswith(".mat"):
                trx = TrxMat(file)
                t = trx["t"]
                data = trx["spine"]
                run, data = next(iter(data.items()))
                if run == "spine":
                    run, data = next(iter(data.items()))
                t = t[run]
            elif file.name.endswith(".csv"):
                if labels.camera_framerate:
                    logging.info(f"camera frame rate: {labels.camera_framerate}fps")
                else:
                    logging.info("assuming 30-fps camera frame rate")
                    labels.camera_framerate = 30
                t, data = fimtrack.read_spines(file, fps=labels.camera_framerate)
                run = "NA"
            else:
                # label files not processed; only their data dependencies are
                logging.info(f"ignoring file: {file.name}")
                continue

            # downsample the skeleton
            if isinstance(data, dict):
                for larva in data:
                    data[larva] = get_5point_spines(data[larva])
            else:
                data = get_5point_spines(data)

            post_filters = model.clf_config.get('post_filters', None)

            # assign labels and apply post-prediction filters
            if isinstance(data, dict):
                ref_length = np.median(np.concatenate([
                    model.body_length(spines) for spines in data.values()
                    ]))
                model.average_body_length = ref_length
                logging.info(f"average body length: {ref_length}")
                for larva, spines in data.items():
                    predictions = model.predict((t[larva], spines))
                    if predictions is None:
                        logging.info(f"failure to window track: {larva}")
                    else:
                        t_ = t[larva]
                        predictions = apply_filters(t_, predictions,
                            post_filters, labels.decoding_label_list, larva)
                        labels[run, larva] = dict(_zip(t_, predictions))
            else:
                ref_length = np.median(model.body_length(data))
                model.average_body_length = ref_length
                logging.info(f"average body length: {ref_length}")
                for larva in np.unique(larvae):
                    mask = larvae == larva
                    predictions = model.predict((t[mask], data[mask]))
                    if predictions is None:
                        logging.info(f"failure to window track: {larva}")
                    else:
                        t_ = t[mask]
                        predictions = apply_filters(t_, predictions,
                            post_filters, labels.decoding_label_list, larva)
                        labels[run, larva] = dict(_zip(t_, predictions))

            # save the predicted labels to file
            labels.dump(get_output_filepath(backend, file))

            done = True


def predict_larva_dataset(backend, model, file, subset="validation", subsets=(.8, .2, 0)):
    from taggingbackends.data.dataset import LarvaDataset
    dataset = LarvaDataset(file, new_generator(), subsets)
    return model.predict(dataset, subset)

def _zip(xs, ys):
    # prevent issues similar to #2
    assert len(xs) == len(ys)
    return zip(xs, ys)

def get_output_filepath(backend, file):
    #if file.is_relative_to(backend.interim_data_dir()): # Py>=3.9
    if str(file).startswith(str(backend.interim_data_dir())):
        subdir = file.parent.relative_to(backend.interim_data_dir())
    else:
        #assert file.is_relative_to(backend.raw_data_dir())
        assert str(file).startswith(str(backend.raw_data_dir()))
        subdir = file.parent.relative_to(backend.raw_data_dir())
    parentdir = backend.processed_data_dir() / subdir
    parentdir.mkdir(parents=True, exist_ok=True)
    target = parentdir / "predicted.label"
    if target.is_file():
        logging.info(f"ouput file already exists: {target}")
        i = 0
        while True:
            i += 1
            target = parentdir / f"predicted-{i}.label"
            if not target.is_file(): break
    return target

def apply_filters(t, actions, post_filters, labels, larva_id):
    # `labels` can be used to map labels to class indices
    # `larva_id` is for logging only; no filters should rely on this datum
    if post_filters:
        for post_filter in post_filters:
            if post_filter == 'ABA->AAA':
                # modify sequentially
                for k in range(1, len(actions)-1):
                    action = actions[k-1]
                    if actions[k] != action and action == actions[k+1]:
                        actions[k] = action
            elif post_filter == 'ABC->AAC':
                # modify sequentially
                for k in range(1, len(actions)-1):
                    if actions[k-1] != actions[k] and actions[k] != actions[k+1]:
                        actions[k] = actions[k-1]
            elif ' with duration' in post_filter and '->' in post_filter:
                condition, replacement = post_filter.split('->')
                replacement = replacement.strip()
                assert replacement in labels
                # the predicted actions are (decoded) labels, not class indices
                #replacement = labels.index(replacement)
                label, constraint = condition.split(' with duration')
                label = label.strip()
                assert label in labels
                assert re.match(r'^ *(<|<=|>|>=|==) *[0-9.]+ ?s? *$', constraint) is not None
                constraint = constraint.rstrip('s ')
                constraint = f'duration{constraint}'
                k = 0
                while k < len(actions):
                    if actions[k] == label:
                        i = k
                        k += 1
                        while k < len(actions) and actions[k] == label:
                            k += 1
                        j = k - 1
                        duration = t[j] - t[i]
                        if eval(constraint):
                            logging.info(f'larva {larva_id}: replacing labels at {k-i} steps ({round(duration, 3)}s): {actions[i]}->{replacement}')
                            for j in range(i, k):
                                actions[j] = replacement
                    else:
                        k += 1
            else:
                raise ValueError(f"filter not supported: {post_filter}")
    return actions


from taggingbackends.main import main

if __name__ == "__main__":
    main(predict_model)

