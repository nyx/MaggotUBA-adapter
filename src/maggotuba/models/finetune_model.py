from taggingbackends.data.labels import Labels
import logging
from taggingbackends.data.dataset import LarvaDataset
from maggotuba.models.trainers import MaggotTrainer, new_generator, enforce_reproducibility, fork_model
import glob

def finetune_model(backend, original_model_instance="default",
                subsets=(1, 0, 0), seed=None, iterations=100, **kwargs):
    # list training data files;
    # we actually expect a single larva_dataset file that make_dataset generated
    # or moved into data/interim/{instance}/
    #larva_dataset_file = backend.list_interim_files("larva_dataset_*.hdf5") # this one is recursive
    larva_dataset_file = glob.glob(str(backend.interim_data_dir() / "larva_dataset_*.hdf5")) # this other one is not recursive
    assert len(larva_dataset_file) == 1

    # instanciate a LarvaDataset object, that is similar to a PyTorch DataLoader
    # add can initialize a Labels object
    # note: subsets=(1, 0, 0) => all data are training data; no validation or test subsets
    dataset = LarvaDataset(larva_dataset_file[0], new_generator(seed),
                           subsets=subsets, **kwargs)

    # initialize a Labels object
    labels = dataset.labels
    assert 0 < len(labels)

    # the labels may be bytes objects; convert to str
    labels = labels if isinstance(labels[0], str) else [s.decode() for s in labels]

    # could be moved into `make_trainer`, but we need it to access the generator
    enforce_reproducibility(dataset.generator)

    # fork the original model
    fork_model(backend, original_model_instance)
    logging.info("model forked")

    # load the forked model
    config_file = backend.list_model_files('clf_config.json')[0]
    model = MaggotTrainer(config_file)
    model.n_pretraining_iter = 0
    model.n_finetuning_iter = iterations

    # fine-tune the model on the loaded dataset
    model.train(dataset)

    # save the model
    print(f"saving model \"{backend.model_instance}\"")
    model.save()


from taggingbackends.main import main

if __name__ == "__main__":
    main(finetune_model)
