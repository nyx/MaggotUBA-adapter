import numpy as np
import torch
import torch.nn as nn
from behavior_model.models.neural_nets import device
from maggotuba.models.modules import SupervisedMaggot, MultiscaleSupervisedMaggot, MaggotBag
from maggotuba.features.preprocess import Preprocessor
from taggingbackends.explorer import BackendExplorer, check_permissions
import logging
import json
import re
import os.path

"""
This model borrows the pre-trained MaggotUBA encoder, substitutes a dense layer
for the decoder, and (re-)trains the entire model.

Attribute `config` refers to MaggotUBA autoencoder.
Attribute `clf_config` refers to the supervised model, both the retrained
encoder and the trained classifier.

Several data preprocessing steps are included for use in prediction mode.
Training the model instead relies on the readily-preprocessed data of a
*larva_dataset hdf5* file.
"""
class MaggotTrainer:
    def __init__(self, cfgfilepath, behaviors=[], n_layers=1, n_iterations=None,
            average_body_length=1.0, device=device):
        self.model = SupervisedMaggot(cfgfilepath, behaviors, n_layers, n_iterations)
        self.preprocessor = Preprocessor(self, average_body_length)
        self.device = device

    @property
    def config(self):
        return self.model.encoder.config

    @property
    def clf_config(self):
        return self.model.clf.config

    @property
    def labels(self):
        return self.model.clf.behavior_labels

    @labels.setter
    def labels(self, labels):
        self.model.clf.behavior_labels = labels

    @property
    def average_body_length(self):
        return self.preprocessor.average_body_length

    @average_body_length.setter
    def average_body_length(self, l):
        self.preprocessor.average_body_length = l

    def body_length(self, data):
        return self.preprocessor.body_length(data)

    def forward(self, x, train=False):
        if train:
            return self.model(x)
        else:
            if isinstance(x, torch.Tensor):
                if x.dtype is not torch.float32:
                    x = x.to(torch.float32)
            else:
                x = torch.from_numpy(x.astype(np.float32))
            y = self.model(x.to(self.device))
            return y.cpu().numpy()

    def prepare_dataset(self, dataset, training=False):
        if training:
            self.model.clf.config['training_dataset'] = self.model.path_for_config(dataset.path)
        try:
            dataset.batch_size
        except AttributeError:
            dataset.batch_size = self.config["batch_size"]
        target_winlen = self.config["len_traj"]
        # 0-indexed midpoint -> first point of the second half if length is even,
        #                       middle point if length is odd
        midpoint = dataset.window_length // 2
        # to help reason about indices, we want:
        # * for a 2-point window: the midpoint and the point before,
        # * for a 3-point window: the midpoint, the point before and the point after,
        # * and so on.
        before = target_winlen // 2
        after = target_winlen // 2 + target_winlen % 2
        assert before + after == target_winlen
        if not (0 <= midpoint - before and midpoint + after <= dataset.window_length):
            raise ValueError(f"the dataset can provide segments of up to {dataset.window_length} time points")
        dataset._mask = slice(midpoint - before, midpoint + after)

    def init_model_for_training(self, dataset):
        kwargs = {}
        if dataset.class_weights is not None:
            kwargs['weight'] = torch.from_numpy(dataset.class_weights.astype(np.float32)).to(self.device)
        self.model.train() # this only sets the model in training mode (enables gradients)
        self.model.to(self.device)
        criterion = nn.CrossEntropyLoss(**kwargs)
        return criterion

    def _pretrain_classifier(self):
        model = self.model
        return model.n_pretraining_iter > 0 and model.encoder.was_pretrained()

    def pretrain_classifier(self, criterion, dataset):
        model = self.model
        grad_clip = self.config['grad_clip']
        optimizer = torch.optim.Adam(model.clf.parameters())
        print("pre-training the classifier...")
        for step in range(model.n_pretraining_iter):
            optimizer.zero_grad()
            # TODO: add an option for renormalizing the input
            data, expected = self.draw(dataset)
            predicted = self.forward(data, train=True)
            loss = criterion(predicted, expected)
            loss.backward()
            nn.utils.clip_grad_norm_(model.clf.parameters(), grad_clip)
            optimizer.step()

    def finetune(self, criterion, dataset):
        model = self.model
        grad_clip = self.config['grad_clip']
        optimizer = torch.optim.Adam(model.parameters())
        print(("fine-tuning" if self._pretrain_classifier() else "training") + \
               " the encoder and classifier...")
        for step in range(model.n_finetuning_iter):
            optimizer.zero_grad()
            data, expected = self.draw(dataset)
            predicted = self.forward(data, train=True)
            loss = criterion(predicted, expected)
            loss.backward()
            nn.utils.clip_grad_norm_(model.parameters(), grad_clip)
            optimizer.step()

    def train(self, dataset):
        self.prepare_dataset(dataset, training=True)
        criterion = self.init_model_for_training(dataset)
        # pre-train the classifier with static encoder weights
        if self._pretrain_classifier():
            self.pretrain_classifier(criterion, dataset)
        # fine-tune both the encoder and the classifier
        self.finetune(criterion, dataset)
        return self

    def draw(self, dataset, subset="train"):
        data, expected = dataset.getbatch(subset)
        if isinstance(data, list):
            data = torch.stack(data)
        data = data.to(torch.float32).to(self.device)
        if isinstance(expected, list):
            expected = torch.stack(expected)
        if subset.startswith("train"):
            expected = expected.to(torch.long).to(self.device)
        return data, expected

    @torch.no_grad()
    def predict(self, data, subset=None):
        model = self.model
        model.eval()
        model.to(self.device)
        if subset is None:
            # data is a (times, spines) couple
            data = self.preprocessor(*data)
            if data is None:
                return
            output = self.forward(data)
            label_ids = np.argmax(output, axis=1)
            labels = [self.labels[label] for label in label_ids]
            return labels
        else:
            dataset = data
            self.prepare_dataset(dataset)
            predicted, expected = [], []
            for data, exp in dataset.getsample(subset, "all"):
                output = self.forward(data)
                pred = np.argmax(output, axis=1)
                exp = exp.numpy()
                assert pred.size == exp.size
                predicted.append(pred)
                expected.append(exp)
            predicted = np.concatenate(predicted)
            expected = np.concatenate(expected)
            predicted = [self.labels[label] for label in predicted]
            expected = [dataset.labels[label] for label in expected]
            return predicted, expected

    def save(self, copy_dataset=True):
        self.model.save()
        if copy_dataset:
            # copy the compiled training dataset into the model directory
            try:
                dataset = self.model.clf.config['training_dataset']
            except KeyError:
                pass
            else:
                copy = self.model.clf.path / os.path.basename(dataset)
                with open(copy, 'wb') as g:
                    with open(dataset, 'rb') as f:
                        g.write(f.read())
                check_permissions(copy)

    @property
    def root_dir(self):
        return self.model.root_dir

    @root_dir.setter
    def root_dir(self, dir):
        self.model.root_dir = dir

    @property
    def n_pretraining_iter(self):
        return self.model.n_pretraining_iter

    @n_pretraining_iter.setter
    def n_pretraining_iter(self, n):
        self.model.clf.config['pretraining_iter'] = n

    @property
    def n_finetuning_iter(self):
        return self.model.n_finetuning_iter

    @n_finetuning_iter.setter
    def n_finetuning_iter(self, n):
        self.model.clf.config['finetuning_iter'] = n

def new_generator(seed=None):
    generator = torch.Generator('cpu')
    if seed == 'random': return generator
    if seed is None: seed = 0b11010111001001101001110
    return generator.manual_seed(seed)

def enforce_reproducibility(generator=None):
    import random
    if generator is None:
        seed = 0b11010111001001101001110
    else:
        seed = generator.initial_seed()
    # see https://pytorch.org/docs/1.13/notes/randomness.html
    torch.use_deterministic_algorithms(True)
    if torch.cuda.is_available:
        torch.backends.cudnn.deterministic = True
    torch.manual_seed(seed)
    seed = seed % 2**32
    np.random.seed(seed)
    random.seed(seed)


class MultiscaleMaggotTrainer(MaggotTrainer):
    def __init__(self, cfgfilepath, behaviors=[], n_layers=1, n_iterations=None,
            average_body_length=1.0, device=device):
        self.model = MultiscaleSupervisedMaggot(cfgfilepath, behaviors,
                                                n_layers, n_iterations)
        self.preprocessor = Preprocessor(self, average_body_length)
        self.device = device
        self._default_encoder_config = None
        # check consistency
        if n_iterations is None:
            ref_config = self.config
            for attr in ["batch_size", "optim_iter"]:
                for enc in self.model.encoders:
                    assert enc.config[attr] == ref_config[attr]

    @property
    def config(self):
        if self._default_encoder_config is None:
            len_traj = 0
            for enc in self.model.encoders:
                cfg = enc.config
                if len_traj < cfg["len_traj"]:
                    len_traj = cfg["len_traj"]
                    self._default_encoder_config = cfg
        return self._default_encoder_config


class MaggotBagging(MaggotTrainer):
    def __init__(self, cfgfilepaths, behaviors=[], n_layers=1,
            average_body_length=1.0, device=device):
        self.model = MaggotBag(cfgfilepaths, behaviors, n_layers)
        self.preprocessor = Preprocessor(self, average_body_length)
        self.device = device


"""
    make_trainer(config_file, ...)
    make_trainer(backend: BackendExplorer, pretrained_model_instance, ...)

Pick the adequate trainer following a rapid inspection of the config file(s).
"""
def make_trainer(first_arg, *args, **kwargs):
    if isinstance(first_arg, BackendExplorer):
        backend = first_arg
        # shift the positional arguments
        assert args
        pretrained_model_instance = args[0]
        args = args[1:]

        if isinstance(pretrained_model_instance, str):
            config_file = import_pretrained_model(backend, pretrained_model_instance)
            model = make_trainer(config_file, *args, **kwargs)
        else:
            pretrained_model_instances = pretrained_model_instance
            config_files = import_pretrained_models(backend, pretrained_model_instances)
            model = make_trainer(config_files, *args, **kwargs)

        model.root_dir = backend.project_dir

    else:
        config_file = first_arg
        #enforce_reproducibility()

        # the type criterion does not fail in the case of unimplemented bagging,
        # as config files are listed in a pretrained_models subdirectory.
        if isinstance(config_file, list): # multiple encoders
            config_files = config_file
            model = MultiscaleMaggotTrainer(config_files, *args, **kwargs)
        else: # single encoder
            model = MaggotTrainer(config_file, *args, **kwargs)

    return model

# TODO: merge the below two functions

"""
The files of the pretrained model are located in the `pretrained_models`
directory. Importing a pretrained model consists in creating a directory in
the `models` directory, named by the instance, and copying the model files.
The train step will make more files in the model instance directory.
"""
def import_pretrained_model(backend, pretrained_model_instance):
    pretrained_autoencoder_dir = backend.project_dir / "pretrained_models" / pretrained_model_instance
    config_file = None
    for file in pretrained_autoencoder_dir.iterdir():
        if not file.is_file():
            continue
        logging.debug(f"copying file: {file}")
        dst = backend.model_dir() / file.name
        if file.name.endswith("config.json"):
            with open(file) as f:
                config = json.load(f)
            dir = backend.model_dir().relative_to(backend.project_dir)
            config["log_dir"] = str(dir)
            logging.debug(f"log_dir: \"{config['log_dir']}\"")
            with open(dst, "w") as f:
                json.dump(config, f, indent=2)
            assert config_file is None
            config_file = dst
        else:
            assert file.name != 'trained_classifier.pt'
            with open(file, "rb") as i:
                with open(dst, "wb") as o:
                    o.write(i.read())
        check_permissions(dst)
    return config_file

def import_pretrained_models(backend, model_instances):
    config_files = []
    for pretrained_model_instance in model_instances:
        pretrained_autoencoder_dir = backend.project_dir / "pretrained_models" / pretrained_model_instance
        encoder_dir = backend.model_dir() / pretrained_model_instance
        encoder_dir.mkdir(exist_ok=True)
        config_file = None
        for file in pretrained_autoencoder_dir.iterdir():
            if not file.is_file():
                continue
            dst = encoder_dir / file.name
            if file.name.endswith("config.json"):
                with open(file) as f:
                    config = json.load(f)
                dir = encoder_dir.relative_to(backend.project_dir)
                config["log_dir"] = str(dir)
                with open(dst, "w") as f:
                    json.dump(config, f, indent=2)
                assert config_file is None
                config_file = dst
            else:
                with open(file, "rb") as i:
                    with open(dst, "wb") as o:
                        o.write(i.read())
            check_permissions(dst)
        assert config_file is not None
        config_files.append(config_file)
    return config_files

"""
Copy a model instance under another instance name.
"""
def fork_model(backend, src_instance):
    srcdir = backend.model_dir(src_instance, False)
    dstdir = backend.model_dir()
    config_files = []
    pattern = f"models/{src_instance}"
    replacement = f"models/{backend.model_instance}"
    for srcfile in srcdir.iterdir():
        if not srcfile.is_file():
            continue
        dstfile = dstdir / srcfile.name
        if srcfile.name.endswith('config.json'):
            with open(srcfile) as f:
                config = json.load(f)
            for element, value in config.items():
                if isinstance(value, str):
                    value = re.sub(pattern, replacement, value)
                    config[element] = value
            with open(dstfile, 'w') as f:
                json.dump(config, f, indent=2)
            config_files.append(srcfile)
        else:
            with open(srcfile, 'rb') as i:
                with open(dstfile, 'wb') as o:
                    o.write(i.read())
        check_permissions(dstfile)
    return config_files


# Julia functions
def searchsortedfirst(xs, x):
    for i, x_ in enumerate(xs):
        if x <= x_:
            return i

def searchsortedlast(xs, x):
    for i in range(len(xs))[::-1]:
        x_ = xs[i]
        if x_ <= x:
            return i
